//
// Created by twak on 07/10/2019.
//

#include "responsive_layout.h"
#include "responsive_label.h"
#include <iostream>

using namespace std;

//Function for header layout for small devices.
void ResponsiveLayout::header1(const QRect &r, int i) {

    QLayoutItem *o = list_.at(i);

    ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

    if (label == NULL) // null: cast failed on pointer
        cout << "warning, unknown widget class in layout" << endl;
    else if (label -> text() == kNavTabs )
        label -> setGeometry(5+r.x(),5+r.y(), 60, 50);
    else if (label -> text() == kHomeLink)
        label -> setGeometry(10 + r.x() + 60, 5 + r.y(), 100, 50);
    else if (label -> text() == kShoppingBasket)
        label -> setGeometry(r.width() - 65 + r.x(), 5 +r.y(), 60,  50);
    else if (label -> text() == kSignIn)
        label -> setGeometry(r.width() - 130 + r.x(), 5 + r.y(), 60, 50);

    else if (label -> text() == kSearchButton)
        label -> setGeometry(r.width() - 65+r.x(), 60 + r.y(),60, 40);
    else if(label -> text() == kSearchText)
        label -> setGeometry(5 + r.x(), 60 + r.y(), r.width() - 75, 40);

    else if(label -> text() == kSearchOptions)
        label -> setGeometry(5 + r.x(), 105 + r.y(), r.width() - 10, 30);

    else if(label-> text() == kAdvert) {
        label->setGeometry(5 + r.x(), 140 + r.y(), r.width() - 10, 80);
    }
}

//Function for header layout for medium devices.
void ResponsiveLayout::header2(const QRect &r, int i) {

    QLayoutItem *o = list_.at(i);

    ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

    if (label == NULL) // null: cast failed on pointer
        cout << "warning, unknown widget class in layout" << endl;
    else if (label -> text() == kNavTabs ) // headers go at the top
        label -> setGeometry(5+r.x(),50+r.y(), 60, 50);
    else if (label -> text() == kHomeLink)
        label -> setGeometry(10 + r.x() + 60, 50 + r.y(), 100, 50);

    else if(label -> text() == kSearchText)
        label -> setGeometry(15 + r.x() + 160, 50 + r.y(), r.width() - 175 -200, 50);
    else if (label -> text() == kSearchButton)
        label -> setGeometry(r.width() - 195 +r.x(), 50 + r.y(),60, 50);

    else if (label -> text() == kShoppingBasket)
        label -> setGeometry(r.width() - 65 + r.x(), 50 +r.y(), 60,  50);
    else if (label -> text() == kSignIn)
        label -> setGeometry(r.width() - 130 + r.x(), 50 + r.y(), 60, 50);

    else if(label -> text() == kSearchOptions)
        label -> setGeometry(5 + r.x(), 105 + r.y(), r.width() - 10, 30);

}

//Function for header layout for large devices.
void ResponsiveLayout::header3(const QRect &r, int i) {

    QLayoutItem *o = list_.at(i);

    ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

    if (label == NULL) // null: cast failed on pointer
        cout << "warning, unknown widget class in layout" << endl;
    else if (label -> text() == kNavTabs ) // headers go at the top
        label -> setGeometry(5+r.x(),90 +r.y(), 80, 100);
    else if (label -> text() == kHomeLink)
        label -> setGeometry(10 + r.x() + 80, 90 + r.y(), 140, 100);

    else if(label -> text() == kSearchText)
        label -> setGeometry(15 + r.x() + 220, 90 + r.y(), r.width() - 325 -250, 100);
    else if (label -> text() == kSearchButton)
        label -> setGeometry(r.width() - 335 +r.x(), 90 + r.y(),80, 100);

    else if (label -> text() == kShoppingBasket)
        label -> setGeometry(r.width() - 250 + r.x(), 90 +r.y(), 100,  100);
    else if (label -> text() == kSignIn)
        label -> setGeometry(r.width() - 145 + r.x(), 90 + r.y(), 140, 100);

}

//Function for base layout of larger devices
void ResponsiveLayout::base(const QRect &r, int i, int resLine) {
    QLayoutItem *o = list_.at(i);

    ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

    if(label->text() == kSearchBackward)
        label->setGeometry(r.x() + (r.width() / 2) - 75, r.y() + 500 + (resLine*440), 60, 60);
    else if(label->text() == kSearchForward)
        label->setGeometry(r.x() + 15 + r.width() /2, r.y() + 500 + (resLine*440), 60, 60);
    else if(label->text() == kFooter)
        label->setGeometry(5 + r.x(), r.y() + 580 + (resLine*440), r.width() - 10, r.height() - (550 + (resLine*440)));

}

//Function for mobile device layout
void ResponsiveLayout::mob(const QRect &r /* our layout should always fit inside r */ ) {

    int countRes = 0, countAd = 0;  //Counters for results and adverts

    QLayout::setGeometry(r);

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        try {
            // cast the widget to one of our responsive labels
            ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

            if (label == NULL) // null: cast failed on pointer
                cout << "warning, unknown widget class in layout" << endl;

            else if (label -> text() == kSearchResult && countRes <= 14 ) {
                label->setGeometry(10 + r.x(), 225 + r.y() + (countRes * 190), r.width() - 20, 180);
                countRes += 1;  //Increment the result counter to be used for later positioning
            }

            else if(label->text() == kSearchBackward)
                label->setGeometry(r.x() + (r.width() / 2) - 35, r.y() + 225 + (countRes*190), 30, 30);
            else if(label->text() == kSearchForward)
                label->setGeometry(r.x() + 5 + r.width() /2, r.y() + 225 + (countRes*190), 30, 30);

            else if(label->text() == kFooter)
                label->setGeometry(5 + r.x(), r.y() + 265 + (countRes*190), r.width() - 10, r.height() - (270 + (countRes*190)));

            else // otherwise: disappear label by moving out of bounds
                label -> setGeometry (-1,-1,0,0);

            header1(r, i);
        }
        catch (bad_cast) {
            // bad_case: cast failed on reference...
            cout << "warning, unknown widget class in layout" << endl;
        }
    }
}

//Function for small tablet layout
void ResponsiveLayout::sTab(const QRect &r) {

    int countRes = 0, countAd = 0, resLine = 0; //Counter for results, adverts and how many lines of results.

    QLayout::setGeometry(r);

    for (int i = 0; i < list_.size(); i++) {

        QLayoutItem *o = list_.at(i);

        try {
            // cast the widget to one of our responsive labels
            ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

            if (label == NULL) // null: cast failed on pointer
                cout << "warning, unknown widget class in layout" << endl;

            else if (label -> text() == kSearchResult && countRes <= 29 ) {
                if(countRes % 2 == 0) {
                    label->setGeometry(10 + r.x(), 225 + r.y() + (resLine * 190), r.width() / 2 - 15, 180);
                    countRes += 1;
                } else if(countRes % 2 == 1) {
                    label->setGeometry(5 + r.x() + r.width() / 2, 225 + r.y() + (resLine * 190), r.width() / 2 - 15, 180);
                    countRes += 1;
                    resLine += 1;   //Increment results after each addition and lines after each new line filled.
                }
            }

            else if(label->text() == kSearchBackward)
                label->setGeometry(r.x() + (r.width() / 2) - 35, r.y() + 225 + (resLine*190), 30, 30);
            else if(label->text() == kSearchForward)
                label->setGeometry(r.x() + 5 + r.width() /2, r.y() + 225 + (resLine*190), 30, 30);

            else if(label->text() == kFooter)
                label->setGeometry(5 + r.x(), r.y() + 265 + (resLine*190), r.width() - 10, r.height() - (270 + (resLine*190)));

            else // otherwise: disappear label by moving out of bounds
                label -> setGeometry (-1,-1,0,0);

            header1(r, i);
        }
        catch (bad_cast) {
            // bad_case: cast failed on reference...
            cout << "warning, unknown widget class in layout" << endl;
        }
    }
}

//Function for medium tablet layout
void ResponsiveLayout::mTab(const QRect &r) {
    int countRes = 0, countAd = 0, resLine = 0; //Counter for results, adverts and lines of results.
    QLayout::setGeometry(r);
    for (int i = 0; i < list_.size(); i++) {
        QLayoutItem *o = list_.at(i);
        try {
            // cast the widget to one of our responsive labels
            ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());

            if (label == NULL) // null: cast failed on pointer
                cout << "warning, unknown widget class in layout" << endl;
            else if(label -> text() == kAdvert) {
                if (countAd == 0) {
                    label->setGeometry(5 + r.x(), 5 + r.y(), r.width() - 10, 40);
                    countAd += 1;
                } else if (countAd == 1) {
                    label->setGeometry(5 + r.x(), 140 + r.y(), r.width() - 10, 80);
                    countAd += 1;   //Increment advert to know which advert we are currently on
                }
            }
            else if (label -> text() == kSearchResult && countRes <= 44 ) {
                if(countRes % 3 == 0) {
                    label->setGeometry(20 + r.x(), 240 + r.y() + (resLine * 240), r.width() / 3 - 27, 220);
                    countRes += 1;
                } else if(countRes % 3 == 1) {
                    label->setGeometry(10 + r.x() + r.width() / 3, 240 + r.y() + (resLine * 240), r.width() / 3 - 27, 220);
                    countRes += 1;
                } else if(countRes % 3 == 2) {
                    label->setGeometry(0 + r.x() + 2 * (r.width() / 3), 240 + r.y() + (resLine * 240), r.width() / 3 - 27, 220);
                    countRes += 1;
                    resLine += 1;   //Increment results after each addition and result lines after each line filled
                }
            }
            else if(label->text() == kSearchBackward)
                label->setGeometry(r.x() + (r.width() / 2) - 35, r.y() + 225 + (resLine*240), 30, 30);
            else if(label->text() == kSearchForward)
                label->setGeometry(r.x() + 5 + r.width() /2, r.y() + 225 + (resLine*240), 30, 30);
            else if(label->text() == kFooter)
                label->setGeometry(5 + r.x(), r.y() + 265 + (resLine*240), r.width() - 10, r.height() - (270 + (resLine*240)));
            else // otherwise: disappear label by moving out of bounds
                label -> setGeometry (-1,-1,0,0);
            header2(r, i);
        }
        catch (bad_cast) {
            // bad_case: cast failed on reference...
            cout << "warning, unknown widget class in layout" << endl;
        }
    }
}

//Function for medium tablet and very small laptops layout
void ResponsiveLayout::lTab(const QRect &r) {
    int countRes = 0, countAd = 0, resLine = 0;     //Counter for results, adverts and lines of results
    QLayout::setGeometry(r);
    for (int i = 0; i < list_.size(); i++) {
        QLayoutItem *o = list_.at(i);
        try {
            // cast the widget to one of our responsive labels
            ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());
            if (label == NULL) // null: cast failed on pointer
                cout << "warning, unknown widget class in layout" << endl;
            else if(label -> text() == kAdvert) {
                if (countAd == 0) {
                    label->setGeometry(5 + r.x(), 5 + r.y(), r.width() - 10, 80);
                    countAd += 1;
                } else if (countAd == 1) {
                    label->setGeometry(r.width() / 4 - 20 + r.x(), 195 + r.y(), r.width() - r.width()/4 + 15, 300);
                    countAd += 1;       //Increment the adverts after each addition to know which advert we are on.
                }
            }
            else if(label->text() == kSearchOptions)
                label->setGeometry(5 + r.x(), 195 + r.y(), r.width() / 4 - 30, 1000);
            else if (label -> text() == kSearchResult && countRes <= 44 ) {
                if(countRes % 3 == 0) {
                    label->setGeometry(r.x() + r.width() / 4, 500 + r.y() + (resLine * 440), r.width() / 4 - 27, 400);
                    countRes += 1;
                } else if(countRes % 3 == 1) {
                    label->setGeometry(r.x() + 2 *(r.width() / 4), 500 + r.y() + (resLine * 440), r.width() / 4 - 30, 400);
                    countRes += 1;
                } else if(countRes % 3 == 2) {
                    label->setGeometry(r.x() + 3 * (r.width() / 4), 500 + r.y() + (resLine * 440), r.width() / 4 - 27, 400);
                    countRes += 1;
                    resLine += 1;   //Increment the results after each addition and the result lines after each line filled.
                }
            }
            else // otherwise: disappear label by moving out of bounds
                label -> setGeometry (-1,-1,0,0);
            header3(r, i);
            base(r, i, resLine);
        }
        catch (bad_cast) {
            // bad_case: cast failed on reference...
            cout << "warning, unknown widget class in layout" << endl;
        }
    }
}

//Function for large tablets and laptops layout
void ResponsiveLayout::sLap(const QRect &r) {
    int countRes = 0, countAd = 0, resLine = 0; //Counter for results, adverts and lines of results.
    QLayout::setGeometry(r);
    for (int i = 0; i < list_.size(); i++) {
        QLayoutItem *o = list_.at(i);
        try {
            // cast the widget to one of our responsive labels
            ResponsiveLabel *label = static_cast<ResponsiveLabel *>(o->widget());
            if (label == NULL) // null: cast failed on pointer
                cout << "warning, unknown widget class in layout" << endl;
            else if(label -> text() == kAdvert) {
                if (countAd == 0) {
                    label->setGeometry(5 + r.x(), 5 + r.y(), r.width() - 10, 80);
                    countAd += 1;
                } else if (countAd == 1) {
                    label->setGeometry(r.width() / 5 - 20 + r.x(), 195 + r.y(), r.width() - r.width()/5 + 15, 300);
                    countAd += 1;   //Increment adverts after each addition so we know which advert we are on
                }
            }
            else if(label->text() == kSearchOptions)
                label->setGeometry(5 + r.x(), 195 + r.y(), r.width() / 5 - 30, 1000);
            else if (label -> text() == kSearchResult && countRes <= 59 ) {
                if(countRes % 4 == 0) {
                    label->setGeometry(r.x() + r.width() / 5, 500 + r.y() + (resLine * 440), r.width() / 5 - 30, 400);
                    countRes += 1;
                } else if(countRes % 4 == 1) {
                    label->setGeometry(r.x() + 2 *(r.width() / 5), 500 + r.y() + (resLine * 440), r.width() / 5 - 30, 400);
                    countRes += 1;
                } else if(countRes % 4 == 2) {
                    label->setGeometry(r.x() + 3 * (r.width() / 5), 500 + r.y() + (resLine * 440), r.width() / 5 - 30, 400);
                    countRes += 1;
                } else if(countRes % 4 == 3) {
                    label->setGeometry(r.x() + 4 * (r.width() / 5), 500 + r.y() + (resLine * 440), r.width() / 5 - 30, 400);
                    countRes += 1;
                    resLine += 1;   //Increment results after each addition and lines of results after each line is filled
                } else // otherwise: disappear label by moving out of bounds
                    label -> setGeometry (-1,-1,0,0);
            }
            base(r, i, resLine);
            header3(r, i);
        }
        catch (bad_cast) {
            // bad_case: cast failed on reference...
            cout << "warning, unknown widget class in layout" << endl;
        }
    }
}

// Function to calculate which layout function to use
void ResponsiveLayout::setGeometry(const QRect &r /* our layout should always fit inside r */ ) {

    QLayout::setGeometry(r);

    //Calling different layout functions based on the size of the device
    if((r.width() <= r.height() && r.width() < 500 ) || (r.height() <= r.width() && r.width() < 500)) {
        mob(r);
    }
    else if((r.width() <= r.height() && r.width() < 700) || (r.height() <= r.width() && r.width() < 700)) {
        sTab(r);
    }
    else if((r.width() <= r.height() && r.width() < 900) || (r.height() <= r.width() && r.width() < 900)) {
        mTab(r);
    }
    else if((r.width() <= r.height() && r.width() < 1300) || (r.height() <= r.width() && r.width() < 1300)) {
        lTab(r);
    }
    else if((r.width() <= r.height() && r.width() < 2000) || (r.height() <= r.width() && r.width() < 2000)) {
        sLap(r);
    }
}


// following methods provide a trivial list-based implementation of the QLayout class
int ResponsiveLayout::count() const {
    return list_.size();
}

QLayoutItem *ResponsiveLayout::itemAt(int idx) const {
    return list_.value(idx);
}

QLayoutItem *ResponsiveLayout::takeAt(int idx) {
    return idx >= 0 && idx < list_.size() ? list_.takeAt(idx) : 0;
}

void ResponsiveLayout::addItem(QLayoutItem *item) {
    list_.append(item);
}

QSize ResponsiveLayout::sizeHint() const {
    return minimumSize();
}

QSize ResponsiveLayout::minimumSize() const {
    return QSize(320,320);
}

ResponsiveLayout::~ResponsiveLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0)))
        delete item;
}
