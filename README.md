# Responsive Qt user interface design

A responsive user interface design written in c++, using Qt widgets.


# Why I made this programm

I made this programm as a project within my 2nd year at university. It was one
of the first times I worked with c++ and it was interesting to see how page
geometry works.


# Frameworks used

    * Qt5.3.1

# Features

    * A respomsive, mobile and desktop friendly user interface design for an
        online store such as Amazon or eBay.
    
    
# How to run on Linux

N/a
    

# Working Examples

Average sized mobile interface:

![mobint](/report/images/rendered375x812.png)


Average sized tablet interface:

![tabint](/report/images/rendered700x1050.png)


Average sized laptop interface:

![lapint](/report/images/rendered1366x1024.png)


All sizes:
[Link 1: All Sizes](https://gitlab.com/lawebb/responsive-ui/-/tree/master/report/images)


# Specification

System Spec:
N/a

